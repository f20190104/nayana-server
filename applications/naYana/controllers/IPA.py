import os
import epitran
from epitran.backoff import Backoff
import textblob
from textblob import TextBlob
import json
import bs4
from bs4 import BeautifulSoup as bs
from MySQLdb import _mysql

def transliterateHindi():
    st = request.body.read().decode('utf-8')
    epi = epitran.Epitran("hin-Deva",ligatures=True)
    IPA = epi.transliterate(st)
    return IPA
def transliterateEng():
    #db=_mysql.connect(host="naYanaPluginServer.mysql.pythonanywhere-services.com",user="naYanaPluginServ", passwd="HardPassword@123",db="naYanaPluginServ$default") 
    st = request.body.read().decode('utf-8')
    epi = epitran.Epitran("eng-Latn",ligatures=True)
    IPA = epi.transliterate(st)
    return IPA

def transliterateEng2():
    db=_mysql.connect(host="naYanaPluginServer.mysql.pythonanywhere-services.com",user="naYanaPluginServ", passwd="HardPassword@123",db="naYanaPluginServ$default"
    st = request.body.read().decode('utf-8')
    arr = st.split(' ')
    epi = epitran.Epitran("eng-Latn",ligatures=True)
    ans =""
    for(word in arr):
        db.query("""Select ipa from ipa where word = %s;""", word)
        r = db.store_result()
        s = r.fetch_row()
        if(len(s)!=0):
            ans += s[0][0].decode('UTF-8') + " "
        else:
            ans+ = epi.transliterate(word)+" "
    return ans

def getIpaDetect():
    st = request.body.read().decode('utf-8')
    inp_json=json.loads(st)
    def getEpitranObjectDict():
        epi_dic={}
        epi_dic["en"]=epitran.Epitran('eng-Latn',ligatures=True)
        epi_dic["hi"]=epitran.Epitran('hin-Deva',ligatures=True)
        epi_dic["ND"]=epitran.backoff.Backoff(['hin-Deva','eng-Latn'])
        return epi_dic
    epi_dict=cache.ram("epi_dict",getEpitranObjectDict,time_expire=300) #store the dictionary in cache for 5 minutes
    response_obj={}
    response_obj["success"]=[]
    response_obj["lang"]=[]
    response_obj["IPA"]=[]
    i=0
    for x in inp_json["str"]:
        lang = TextBlob(x)
        try:
            key=lang.detect_language()
        except Exception:
            key="en" #not detected

        response_obj["success"].append(0)
        response_obj["lang"].append(key)
        response_obj["IPA"].append("")

        if key in epi_dict.keys():
            try:
                response_obj["IPA"][i]=epi_dict[key].transliterate(x)
                response_obj["success"][i]=1
            except:
                pass
        i+=1

    response.headers["Content-Type"]='application/json'
    response.headers["Access-Control-Allow-Origin"]="*"
    response.headers["Access-Control-Allow-Credentials"]="true"

    return json.dumps(response_obj)

def fontFile():
    response.headers["Access-Control-Allow-Origin"]="*"
    response.headers["Access-Control-Allow-Credentials"]="true"
    path = os.path.join(request.folder, 'static', 'naYanakamikRegular.otf')
    return response.stream(open(path,'rb'), attachment=True)

def getIpaHtml():
    html_doc = request.body.read().decode('utf-8')
    soup = bs(html_doc, 'html.parser')
    def getEpitranObjectDict():
        epi_dic={}
        epi_dic["en"]=epitran.Epitran('eng-Latn',ligatures=True)
        epi_dic["hi"]=epitran.Epitran('hin-Deva',ligatures=True)
        epi_dic["ND"]=epitran.backoff.Backoff(['hin-Deva','eng-Latn'])
        return epi_dic
    epi_dict=cache.ram("epi_dict",getEpitranObjectDict,time_expire=300) #store the dictionary in cache for 5 minutes
    def gettoall1(soup):
        try:
            for x in range(len(soup.contents)):
                gettoall1(soup.contents[x])
        except:
            s=str(soup.string)
            s=epi_dict["en"].transliterate(s)
            soup.replace_with(s)

    gettoall1(soup)
    return str(soup)
